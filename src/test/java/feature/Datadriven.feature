
Feature: Create a Lead api by dynamically taking token from tokencall feature file

  Background:
    * url 'https://tmobile-heroku-csf-stag.herokuapp.com'
    * def tokenresponse = call read('Tokencall.feature')
    * def token = 'Bearer ' + tokenresponse.response.auth.access_token
    * print token

Scenario Outline: Creating an LeadAPI <testno>
    Given path '/service/lead/upsert'
    Given header Authorization = token
    And header Accept = 'application/json'
    And header Content-Type = 'application/json'
    And request {"firstName":"<firstname>","lastName":"<Lastname>","LeadSource":"Join","offer_details__c":" and get FREE overnight shipping","Company":"ABC Company","phoneNumber":"","Email" : "<emailid>","notes": "check notes2 from b2b appointment create","zip":"77755","Shopper_Pin__c":"VIP345679","Consent_Text__c":"Please update","Consent__c":"false" ,"updateRecord":"true","channel":"T-Mobile.com Business","reason":"Just looking","storeId" : "8615","AdobeCampaignId" : "3456ugfdsasde"}
    When method post
    Then status 200
    Then print response.leadId

  Examples:
|  testno | firstname | Lastname | emailid             |
|  1      | Kdemo01   |  Stag1   | Kdemo01@stag1.com   |
|  2      | Kdemo02   |  Stag1   | Kdemo02@stag1.com   |
|  3      | Kdemo03   |  Stag1   | Kdemo03@stag1.com   |

