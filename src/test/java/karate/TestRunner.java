package karate;
import com.intuit.karate.KarateOptions;
import com.intuit.karate.Results;
import com.intuit.karate.Runner;
import com.intuit.karate.junit5.Karate;
import com.intuit.karate.junit5.Karate.Test;


import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;



//@KarateOptions(features = {
    //    "classpath:feature"},
    //    tags = {"@srilatha"}


//)
public class TestRunner {


    @Test
    public void testParallel() {
        String envType=   System.getProperty("karate.env");
        String karateOutputPath = "target/cucumber-html-reports";
        ArrayList<String> tags= new ArrayList<>();
        ArrayList<String> paths= new ArrayList<>();
        System.out.println("printing--->"+envType);
          if (envType.equals("dev")){
        tags.add("@sanity");
         }else if(envType.equals("qa")){
        tags.add("@srilatha");
        }
        paths.add("classpath:feature");
        //paths.add("classpath:CreateEng-Serial.feature");
        System.out.println("printing tags value--->"+tags);
        Results results = Runner.parallel(tags,paths,10,karateOutputPath);
        generateReport(results.getReportDir());
    }

    public void generateReport(String karateOutputPath) {
        Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] { "json" }, true);
        List<String> jsonPaths = new ArrayList<String>(jsonFiles.size());
        jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
        Configuration config = new Configuration(new File("target"), "Karate BDD");
        config.addClassifications("Version", "release/1.0");
        ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
        reportBuilder.generateReports();
    }




}
