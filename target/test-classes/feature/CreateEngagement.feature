Feature: Create a Lead api by passing token dynamically & reading data file static values

  Background:
    * url 'https://tmobile-heroku-csf-stag.herokuapp.com'
    * def lead_api = call read('LeadCreate.feature')
    * def lead_id = lead_api.response.leadId
    * print lead_id
    * def token_response = call read('Tokencall.feature')
    * def token = 'Bearer ' + token_response.response.auth.access_token
    * print token


  Scenario: Creating an LeadAPI
    Given path '/service/engagement/upsert'
    Given header Authorization = token
    And header channel = 'Home Internet'
    And header Content-Type = 'application/json'
    And request read ('engdata.json')
    When method post
    Then status 200
    Then match response.returnCode == 0

